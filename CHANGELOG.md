# pmd-apex analyzer changelog

## v2.12.0
- Update report dependency in order to use the report schema version 14.0.0 (!54)

## v2.11.2
- Update PMD to v6.32.0 (!53)

## v2.11.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!51)

## v2.11.0
- Update base image to adoptopenjdk/openjdk15:alpine (!50)
- Update PMD to v6.30.0 (!50)

## v2.10.0
- Update common to v2.22.0 (!49)
- Update urfave/cli to v2.3.0 (!49)

## v2.9.0
- Update PMD to v6.29.0 (!48)
- Update golang dependencies logrus to v1.6.0, cli to v1.22.5 (!48)

## v2.8.0
- Update common and enabled disablement of rulesets (!47)

## v2.7.1
- Update golang dependencies (!41)

## v2.7.0
- Upgrade PMD to 6.27.0 (!39)
- Update golang dependencies (!39)

## v2.6.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!37)

## v2.5.1
- Update golang to v1.15 (!34)
- Bump PMD to 6.26.0

## v2.5.0
- Add scan object to report (!30)

## v2.4.3
- Dedupe report findings (!28)

## v2.4.2
- Safe handle plugin.Match file closer (!29)

## v2.4.1
- Bump PMD to 6.25.0 (!24)

## v2.4.0
- Switch to the MIT Expat license (!23)

## v2.3.1
- Update Debug output to give a better description of command that was ran (!21)

## v2.3.0
- Update logging to be standardized across analyzers (!20)

## v2.2.4
- Bump PMD to 6.24.0 (!18)

## v2.2.3
- Remove `location.dependency` from the generated SAST report (!17)

## v2.2.2
- Bump PMD to 6.23.0, Docker image to Java 13 (!16)

## v2.2.1
- Bump PMD to 6.22.0 (!14)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!11)

## v2.1.0
- Add support for custom CA certs (!9)

## v2.0.1
- Bump PMD to 6.17.0

## v2.0.0
- Initial release
