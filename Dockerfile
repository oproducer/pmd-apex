FROM golang:1.15 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM adoptopenjdk/openjdk15:alpine

RUN apk update
RUN apk add --no-cache bash zip
RUN apk upgrade

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-6.32.0}

ADD https://github.com/pmd/pmd/releases/download/pmd_releases%2F$SCANNER_VERSION/pmd-bin-$SCANNER_VERSION.zip pmd.zip

RUN unzip -n pmd.zip && \
  rm -f pmd.zip && \
  mv pmd-bin-$SCANNER_VERSION /pmd

# Install analyzer
COPY --from=build /go/src/app/analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
