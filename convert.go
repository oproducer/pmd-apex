package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

// Report is a pmd-generated XML report
type Report struct {
	XMLName xml.Name  `xml:"pmd"`
	Files   []XMLFile `xml:"file"`
}

// XMLFile is an XML node containing violations by file
type XMLFile struct {
	XMLName    xml.Name    `xml:"file"`
	Name       string      `xml:"name,attr"`
	Violations []Violation `xml:"violation"`
}

// Violation is a single instance of a potential vulnerability
type Violation struct {
	XMLName         xml.Name `xml:"violation"`
	BeginLine       int      `xml:"beginline,attr"`
	EndLine         int      `xml:"endline,attr"`
	BeginColumn     int      `xml:"begincolumn,attr"`
	EndColumn       int      `xml:"endcolumn,attr"`
	Rule            string   `xml:"rule,attr"`
	Ruleset         string   `xml:"ruleset,attr"`
	ExternalInfoURL string   `xml:"externalInfoUrl,attr"`
	Priority        int      `xml:"priority,attr"`
	Solution        string   `xml:",chardata"`
}

// TODO: This is an expensive traversal, we should refactor
// our convert to populate on decode
func findFilePathForViolation(r Report, v Violation) string {
	for _, f := range r.Files {
		for _, violation := range f.Violations {
			if violation == v {
				return f.Name
			}
		}
	}

	return ""
}

func (v Violation) severity() report.SeverityLevel {
	switch v.Priority {
	case 1:
		return report.SeverityLevelCritical
	case 2:
		return report.SeverityLevelHigh
	case 3:
		return report.SeverityLevelMedium
	case 4, 5:
		return report.SeverityLevelLow
	}
	return report.SeverityLevelUnknown
}

func (v Violation) file(root string, r Report) string {
	root = strings.TrimSuffix(root, "/") + "/" // Make sure there's a trailing slash
	path := findFilePathForViolation(r, v)
	return strings.TrimPrefix(path, root)
}

func (v Violation) ruleID() string {
	return strings.Join([]string{v.Ruleset, v.Rule}, "-")
}

func (v Violation) compareKey(file string) string {
	fields := []string{
		"PMD",
		v.ruleID(),
		file,
		strconv.Itoa(v.BeginLine),
		strconv.Itoa(v.BeginColumn),
	}
	return strings.Join(fields, ":")
}

func (v Violation) solution() string {
	return strings.TrimSpace(v.Solution)
}

func (v Violation) location(file string) report.Location {
	return report.Location{
		File:      file,
		LineStart: v.BeginLine,
		LineEnd:   v.EndLine,
	}
}

func (v Violation) identifiers() []report.Identifier {
	return []report.Identifier{
		report.Identifier{
			Type:  "pmd_apex_rule_id",
			Name:  fmt.Sprintf("PMD Apex Rule ID %s", v.ruleID()),
			Value: v.ruleID(),
			URL:   v.ExternalInfoURL,
		},
	}
}

var descriptions = map[string]string{
	"Security-ApexBadCrypto":             "The rule makes sure you are using randomly generated IVs and keys for Crypto calls. Hard-wiring these values greatly compromises the security of encrypted data.",
	"Security-ApexCRUDViolation":         "The rule validates you are checking for access permissions before a SOQL/SOSL/DML operation. Since Apex runs in system mode not having proper permissions checks results in escalation of privilege and may produce runtime errors. This check forces you to handle such scenarios.",
	"Security-ApexCSRF":                  "Check to avoid making DML operations in Apex class constructor/init method. This prevents modification of the database just by accessing a page.",
	"Security-ApexDangerousMethods":      "Checks against calling dangerous methods. For the time being, it reports: Against FinancialForce’s 'Configuration.disableTriggerCRUDSecurity()'. Disabling CRUD security opens the door to several attacks and requires manual validation, which is unreliable. Calling 'System.debug' passing sensitive data as parameter, which could lead to exposure of private data.",
	"Security-ApexInsecureEndpoint":      "Checks against accessing endpoints under plain http. You should always use https for security.",
	"Security-ApexOpenRedirect":          "Checks against redirects to user-controlled locations. This prevents attackers from redirecting users to phishing sites.",
	"Security-ApexSharingViolations":     "Detect classes declared without explicit sharing mode if DML methods are used. This forces the developer to take access restrictions into account before modifying objects.",
	"Security-ApexSOQLInjection":         "Detects the usage of untrusted / unescaped variables in DML queries.",
	"Security-ApexSuggestUsingNamedCred": "Detects hardcoded credentials used in requests to an endpoint. You should refrain from hardcoding credentials: They are hard to mantain by being mixed in application code, Particularly hard to update them when used from different classes, Granting a developer access to the codebase means granting knowledge of credentials, keeping a two-level access is not possible, Using different credentials for different environments is troublesome and error-prone. Instead, you should use Named Credentials and a callout endpoint.",
	"Security-ApexXSSFromEscapeFalse":    "Reports on calls to 'addError' with disabled escaping. The message passed to 'addError' will be displayed directly to the user in the UI, making it prime ground for XSS attacks if unescaped.",
	"Security-ApexXSSFromURLParam":       "Makes sure that all values obtained from URL parameters are properly escaped / sanitized to avoid XSS attacks.",
}

var rulelinks = map[string]string{
	"Security-ApexSuggestUsingNamedCred": "https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_callouts_named_credentials.htm",
}

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	// HACK: extract root path from environment variables
	root := os.Getenv("ANALYZER_TARGET_DIR")
	if root == "" {
		root = os.Getenv("CI_PROJECT_DIR")
	}

	var doc Report

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	vulns := make([]report.Vulnerability, 0)
	for _, f := range doc.Files {
		for _, v := range f.Violations {
			file := filepath.Join(prependPath, v.file(root, doc))

			links := make([]report.Link, 0)
			if l, ok := rulelinks[v.ruleID()]; ok {
				links = report.NewLinks(l)
			}

			newVuln := report.Vulnerability{
				Category:    metadata.Type,
				Scanner:     metadata.IssueScanner,
				Name:        v.Rule,
				Message:     v.Rule,
				Description: descriptions[v.ruleID()],
				Solution:    v.solution(),
				Severity:    v.severity(),
				Location:    v.location(file),
				CompareKey:  v.compareKey(file),
				Identifiers: v.identifiers(),
				Links:       links,
			}
			vulns = append(vulns, newVuln)
		}
	}

	newReport := report.NewReport()
	newReport.Vulnerabilities = vulns
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	// PMD can report duplicate vulnerabilities, so we dedupe here
	newReport.Dedupe()
	return &newReport, nil
}
